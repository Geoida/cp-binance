import os
import signal
import asyncio
import logging
import argparse

from typing import Tuple

import binance_checker.binance_threshold_checker
from pubsub.subscriber import Subscriber
from binance_checker.binance_threshold_checker import BinanceThresholdChecker


def setup_logger() -> None:
    """ Setup logger with 2 handlers, file with level set to DEBUG and stdout with level set to WARNING """

    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    sh = logging.StreamHandler()
    sh.setLevel(logging.WARNING)
    sh.setFormatter(formatter)

    fh = logging.FileHandler("cp_binance.log")
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    log.addHandler(sh)
    log.addHandler(fh)


def parse_args() -> Tuple[str, str]:
    """
    Parse input arguments from user

    Returns:
        2-element tuple containing symbol and threshold
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--symbol", help="Symbol to check trade stream for", action="store")
    parser.add_argument("-t", "--threshold", help="Threshold to pop message when found trade with higher price",
                        action="store")
    arguments = parser.parse_args()

    return arguments.symbol, arguments.threshold


def parse_env() -> Tuple[str, str, bool]:
    """
    Check environment variables for:
        BNNC_SYMBOL - symbol to look for,
        BNNC_THRESHOLD - threshold to look for,
        GOOGLE_APPLICATION_CREDENTIALS - path to key in json file

    Returns:
        3-element tuple containing symbol, threshold, google_app
    """
    s = os.environ.get("BNNC_SYMBOL")
    t = os.environ.get("BNNC_THRESHOLD")
    ga = bool(os.environ.get("GOOGLE_APPLICATION_CREDENTIALS"))

    return s, t, ga


def ask_for_params() -> Tuple[str, str]:
    """
    Waits for user to provide new symbol and price

    Returns:
        Tuple of two strings (symbol, threshold)

    """
    s, t = None, None
    while not s and not t:
        try:
            s = input("Please provide a symbol: ")
            t = input("Please provide a price: ")
        except UnicodeDecodeError:
            pass

    return s, t


async def run_app(checker: binance_checker.binance_threshold_checker.BinanceThresholdChecker, g_apps: bool) -> None:
    """
    Run Subscriber if enabled and threshold checker

    Args:
        checker: instance of checker class
        g_apps: enable google apps flow
    """

    if g_apps:
        sub = Subscriber()

    await checker.run()

    if g_apps:
        sub.stop()


def main_loop() -> None:
    def stop(_, __):
        checker.stop()
    env_symbol, env_threshold, google_app = parse_env()
    symbol, threshold = parse_args()

    if not all([symbol, threshold]) and not all([env_symbol, env_threshold]):
        symbol, threshold = ask_for_params()

    signal.signal(signal.SIGINT, stop)
    signal.signal(signal.SIGTERM, stop)

    symbol = symbol or env_symbol
    threshold = threshold or env_threshold

    t = None
    while not t:
        checker = BinanceThresholdChecker(symbol, float(threshold), google_app)
        t = asyncio.run(run_app(checker, google_app))
        symbol, threshold = ask_for_params()


if __name__ == "__main__":
    setup_logger()

    main_loop()
