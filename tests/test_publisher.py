import unittest

from mock.mock import patch, Mock

from pubsub.publisher import Publisher


class TestPublisher(unittest.TestCase):
    def test_publisher(self):
        with patch("google.cloud.pubsub_v1.PublisherClient") as publisher_mock:
            pub = Publisher()
            publisher_mock.assert_called_once()

            data = Mock()
            pub.pub.publish.return_value = data
            pub.publish(data)

            data.encode.assert_called_with("utf-8")
            pub.pub.publish.assert_called_once()
            data.result.assert_called()

