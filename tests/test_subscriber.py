import unittest

from mock.mock import patch, Mock

from pubsub.subscriber import Subscriber


class TestSubscriber(unittest.TestCase):
    def test_subscriber(self):
        with patch("google.cloud.pubsub_v1.SubscriberClient") as subscriber_mock:
            sub = Subscriber()
            subscriber_mock.assert_called_once()

            with self.assertLogs() as captured:
                msg = Mock()
                sub.callback(message=msg)
                self.assertEqual(len(captured.records), 1)
                msg.ack.called_once()

            sub.stop()
            sub.sub.close.assert_called_once()
            sub.sub.delete_subscription.assert_called_once()

