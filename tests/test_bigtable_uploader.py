import unittest

from mock.mock import patch

from consts import PROJECT_ID
from bigtable_uploader.bigtable_uploader import BigTableUploader


class TestBigtableUploader(unittest.TestCase):
    def test_bigtalbe_uploader(self):
        with patch("google.cloud.bigtable.Client") as client_mock:
            bt = BigTableUploader()
            client_mock.assert_called_once_with(project=PROJECT_ID, admin=True)

            table = bt.instance.table("sym")
            bt.add_row("sym", 1.0)
            table.exists.assert_called()
            table.create.assert_not_called()
            table.direct_row.assert_called_once()
            table.mutate_rows.assert_called_once()
