import datetime

from typing import Union

from google.cloud import bigtable
from google.cloud.bigtable import column_family

from consts import PROJECT_ID, INSTANCE_ID


class BigTableUploader:
    """ Class for uploading single binance trade data into BigTable row"""
    def __init__(self) -> None:
        """ Initialize BigTable client """
        self.client = bigtable.Client(project=PROJECT_ID, admin=True)
        self.instance = self.client.instance(INSTANCE_ID)

    def add_row(self, symbol: str, price: Union[str, float]) -> None:
        """
        Add single row to BigTable table

        Args:
            symbol: currency name which equals table id
            price: price from transaction
        """
        table = self.instance.table(symbol)
        max_versions_rule = column_family.MaxVersionsGCRule(2)
        column_family_id = "symbol"
        column_families = {column_family_id: max_versions_rule}
        if not table.exists():
            table.create(column_families=column_families)

        column = "trade_price".encode()
        row_key = f"{symbol}#{datetime.datetime.now().timestamp()}".encode()

        row = table.direct_row(row_key)
        row.set_cell(
            column_family_id, column, price, timestamp=datetime.datetime.utcnow()
        )

        table.mutate_rows([row])
