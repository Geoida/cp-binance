import logging

from google.cloud import pubsub_v1

from consts import PROJECT_ID, SUBSCRIPTION_ID


class Subscriber:
    """ Class for listening for incoming Google PubSub messages """

    def __init__(self) -> None:
        """ Initialize subscriber client and subscribe """
        self.sub = pubsub_v1.SubscriberClient()
        self.subscription_path = self.sub.subscription_path(PROJECT_ID, SUBSCRIPTION_ID)
        self.streaming_pull_future = self.sub.subscribe(self.subscription_path, callback=self.callback)

    def stop(self) -> None:
        """ Stop subscriber """
        self.sub.close()
        self.sub.delete_subscription(request={"subscription": self.subscription_path})

    def callback(self, message: pubsub_v1.subscriber.message.Message) -> None:
        """ Actions to do when a new message is received - in this case just show it to user and acknowledge message"""
        logging.warning(f"NEW TRANSACTION {message.data.decode('utf-8')}")
        message.ack()
