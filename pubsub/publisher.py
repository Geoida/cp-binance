from google.cloud import pubsub_v1

from consts import PROJECT_ID, TOPIC_ID


class Publisher:
    """ Class for publishing Google PubSub messages """

    def __init__(self) -> None:
        """ Initialize publisher and set topic path """
        self.pub = pubsub_v1.PublisherClient()
        self.topic_path = self.pub.topic_path(PROJECT_ID, TOPIC_ID)

    def publish(self, msg: str) -> None:
        """
        Publish provided as an argument message via Google PubSub

        Args:
            msg: message to publish
        """
        msg_encoded = msg.encode("utf-8")
        future = self.pub.publish(self.topic_path, msg_encoded)
        future.result()
