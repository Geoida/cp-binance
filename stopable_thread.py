import threading


class StoppableThread(threading.Thread):

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._stop_event = threading.Event()

    def stop(self) -> None:
        self._stop_event.set()

    def reset(self) -> None:
        self._stop_event = threading.Event()

    def stopped(self) -> bool:
        return self._stop_event.is_set()

