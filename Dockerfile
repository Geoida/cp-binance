FROM python:3.10.5

RUN mkdir /cp_challenge
WORKDIR /cp_challenge

COPY . /cp_challenge/

RUN pip install -r requirements.txt
