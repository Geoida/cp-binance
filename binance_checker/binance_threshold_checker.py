import json
import logging

import websockets

from consts import STREAM_BASE_URL
from pubsub.publisher import Publisher
from bigtable_uploader.bigtable_uploader import BigTableUploader
from stopable_thread import StoppableThread


class StopCheckerExcpetion(Exception):
    pass


class BinanceThresholdChecker(StoppableThread):
    """
    Class for subscribing binance specific (for provided symbol) trade stream and showing
    trades that price is above threshold
    """

    def __init__(self, symbol: str, threshold: float, google_app: bool = False, *args, **kwargs) -> None:
        """
        Initialize checker and start watching trade stream entries

        Args:
            symbol: symbol which specifies stream
            threshold: number that is the lowest price not shown
            google_app: enable/disable pubsub flow and bigtable upload
        """
        super().__init__(*args, **kwargs)
        self.symbol = symbol.lower()
        self.threshold = float(threshold)
        self.req_id = 1
        self.google_app = google_app
        if self.google_app:
            self.pub = Publisher()
            self.bt_uploader = BigTableUploader()

    async def publish_high_price(self, msg: str) -> None:
        """
        Provide information to user in which trade the price went above threshold
        Depending on self.pubsub sends message via PubSub if enabled or only log if disabled

        Args:
            msg: details of transaction with price above threshold
        """
        if self.google_app:
            self.pub.publish(f"Found price higher than threshold={self.threshold}!\n"
                             f"Trade data: {msg}")
        else:
            logging.warning(f"Price has gone above threshold={self.threshold} in trade:\n"
                            f"{msg}")

    async def process_msg(self, msg: str) -> None:
        """
        Log all received messages and check if trade price is above threshold

        Args:
            msg: single message received from binance websocket
        """
        msg = json.loads(msg)
        if self.google_app:
            self.bt_uploader.add_row(self.symbol, msg["p"])

        if float(msg.get("p", -1)) > self.threshold:
            await self.publish_high_price(msg)

    async def init_ws(self) -> None:
        """ Initialize websocket connection and listen for all incoming messages """
        async for ws in websockets.connect(f"{STREAM_BASE_URL}{self.symbol}@trade"):
            try:
                async for message in ws:
                    if self.stopped():
                        return
                    await self.process_msg(message)
            except websockets.ConnectionClosed:  # reconnect automatically on errors
                continue

    async def run(self):
        """ Run untill stopped """
        self.reset()
        while not self.stopped():
            await self.init_ws()
