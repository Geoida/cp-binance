# CP Binance

Application to watch over one symbol binance trade stream 

To use the application build docker image. From project home directory run: "docker build -t cp_challenge .". 

Run a docker with built image and enter /cp_challenge directory.
execute python script "cp_challenge.py"

You can specify symbol and number as env variables _BNNC_SYMBOL_ and _BNNC_THRESHOLD_
or by passing them as script arguments.

App always check for environment variable _GOOGLE_APPLICATION_CREDENTIALS_. If this one is set the APP will try using pubsub to show trades with higher price than provided, and it will also save all the data to BigTable.

To enter new symbol/price press CTRL+C and wait a little for threads to finish working.